package com.esewa.model;

import java.util.List;

/**
 * @Author Reshma
 * @Date 6/27/2018
 * @Month 06
 **/
public class BookCollection {
    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    private List<Book> bookList;
    public Book getFirstBook(){
        return bookList.get(0);
    }
}
