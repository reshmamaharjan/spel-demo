package com.esewa.model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import java.util.List;

/**
 * @Author Reshma
 * @Date 6/27/2018
 * @Month 06
 **/
public class Test {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/esewa/model/applicationContext.xml");
        BookLibrary b=(BookLibrary) ac.getBean("bookLibrary");
        List<Book> allBooks= b.getAllBook();
        for(Book book:allBooks){
            System.out.println("BookId:"+book.getBookId()+"\tBook Name:"+book.getBookName());
        }
        Book book=b.getGetFirstBook();
        System.out.println("First Book is::\n"+"BookID:"+book.getBookId()+"Book Name:"+book.getBookName());


    }
}
