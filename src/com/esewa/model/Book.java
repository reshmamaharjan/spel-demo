package com.esewa.model;

/**
 * @Author Reshma
 * @Date 6/27/2018
 * @Month 06
 **/
public class Book {
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int bookId;
    public String bookName;

}

