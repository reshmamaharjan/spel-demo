package com.esewa.model;

import java.util.List;

/**
 * @Author Reshma
 * @Date 6/27/2018
 * @Month 06
 **/
public class BookLibrary {
    public List<Book> getAllBook() {
        return allBook;
    }

    public void setAllBook(List<Book> allBook) {
        this.allBook = allBook;
    }

    public Book getGetFirstBook() {
        return getFirstBook;
    }

    public void setGetFirstBook(Book getFirstBook) {
        this.getFirstBook = getFirstBook;
    }

    private List<Book> allBook;
    private  Book getFirstBook;
}
